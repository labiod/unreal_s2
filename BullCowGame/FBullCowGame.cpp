#pragma once 

#include "FBullCowGame.h"
#include <map>

// to make syntax Unreal friendly
#define TMap std::map 

FBullCowGame::FBullCowGame() { Reset(); }

int32 FBullCowGame::GetCurrentTry() const{ return MyCurrentTry; }

int32 FBullCowGame::GetHiddenWordLength() const { return MyHiddenWord.length(); }

bool FBullCowGame::IsGameWon() const { return bGameWon; }


int32 FBullCowGame::GetMaxTries() const { 
	TMap<int32, int32> WordLengthToMacTries{ {3,4}, {4,7}, {5,10}, {6,15}, {7,20} };
	return WordLengthToMacTries[MyHiddenWord.length()]; 
}

EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess) const
{
	if (!IsIsogram(Guess)) // if guess isn't an isogram 
	{
		return EGuessStatus::NOT_ISOGRAM;
	}
	else if (!IsLowercase(Guess)) // if gues isn't all lowercase
	{
		return EGuessStatus::NOT_LOWERCASE;
	}
	else if (Guess.length() != GetHiddenWordLength())
	{
		return EGuessStatus::WRONG_LENGTH;
	}
	else {
		return EGuessStatus::OK;
	}
}

void FBullCowGame::Reset()
{
	const FString HIDDEN_WORD = "planet"; // this MUST be an isogram
	MyHiddenWord = HIDDEN_WORD;
	MyCurrentTry = 1;
	bGameWon = false;
	return;
}

// receives a VALID guess, increments turn, and returns count
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	MyCurrentTry++;
	FBullCowCount BullCowCount;

	// loop through all letters in the hidden word
	int32 WordLength = MyHiddenWord.length();
	for (int32 MHWChar = 0; MHWChar < WordLength; ++MHWChar)
	{
		// compare letters against the guess
		for (int32 GChar = 0; GChar < WordLength; ++GChar)
		{
			// if they match then
			if (MyHiddenWord[MHWChar] == Guess[GChar])
			{
				// if they're in the same place
				if (MHWChar == GChar) 
				{ 
					BullCowCount.Bulls++; // increment bulls
				}
				else
				{
					BullCowCount.Cows++; // must be a cow
				}
			}
		}
	}
	bGameWon = BullCowCount.Bulls == WordLength;
	return BullCowCount;
}

bool FBullCowGame::IsIsogram(FString Word) const
{
	// treat 0 and 1 letter words as isogramp
	if (Word.length() <= 1) { return true; }
	
	TMap<char, bool> LetterSeen; // setup our map
	for (auto Letter : Word) //for all letters of the word
	{
		Letter = tolower(Letter); // handle mix case
		if (LetterSeen[Letter]) // if the letter is in the map
		{
			// we do NOT have an isogram
			return false;
		}
		else
		{
			LetterSeen[Letter] = true; // add the letter to the map as seen
		}
	}
	return true; // for example in cases where /0 is entered
}

bool FBullCowGame::IsLowercase(FString Word) const
{
	for (auto Letter : Word) // loop through all letter of the word
	{
		if (!islower(Letter))
		{
			return false;
		}
	}
	return true;
}
