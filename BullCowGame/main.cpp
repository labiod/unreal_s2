/* This is the console executable, that makes use of BullCow class
This acts as the view in a MVC pattern, and is responsible for all 
use interaction. For game logic see the FBullCowGame class.
*/
#pragma once

#include <iostream>
#include <string>
#include "FBullCowGame.h"

// to make syntax Unreal friendly
using FText = std::string;
using int32 = int;

// function prototypes as outside a class
void PrintIntro();
void PlayGame();
FText GetValidGuess();
bool AskToPlayAgain();
void PrintGameSummary();

FBullCowGame BCGame; // isntantiate a new game

//the entry point for our application
int main()
{
	bool bPlayAgain = false;
	do
	{
		PrintIntro();
		PlayGame();
		bPlayAgain = AskToPlayAgain();
	} 
	while (bPlayAgain);
	return 0;
}

void PrintIntro()
{
	std::cout << "\nWelcome to Bulls and Cows, a fun word game\n";
	std::cout << std::endl;
	std::cout << "          }   {                ___  " << std::endl;
	std::cout << "          (o o)               (o o) " << std::endl;
	std::cout << "   /-------\\ /                 \\ /-------\\ " << std::endl;
	std::cout << "  / | BULL |0                   0| COW  | \\ " << std::endl;
	std::cout << " *  |-,--- |                     |------|  * " << std::endl;
	std::cout << "    ^      ^                     ^      ^ " << std::endl;
	std::cout << "Can you guess the " << BCGame.GetHiddenWordLength();
	std::cout << " letter isogram I'm thinking of?\n";
	std::cout << std::endl;
	return;
}

// plays a single game to completion
void PlayGame()
{
	BCGame.Reset();
	int32 MaxTries = BCGame.GetMaxTries();

	// loop asking for guesses while the game 
	// is not won and there are still turn
	while (!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= BCGame.GetMaxTries())
	{
		FText Guess = GetValidGuess();

		// Submit valid guess to the game and receive counts
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);
		// print number of bulls and Cows
		std::cout << "Bulls =  " << BullCowCount.Bulls;
		std::cout << ". Cows =  " <<BullCowCount.Cows << "\n\n";
	}
	
	PrintGameSummary();
}

FText GetValidGuess() 
{
	//get a guess from the player
	EGuessStatus Status = EGuessStatus::INVALID_STATUS;
	FText Guess = "";
	do 
	{
		std::cout << "Try " << BCGame.GetCurrentTry() << " of " << BCGame.GetMaxTries();
		std::cout << ". Enter your guess : ";
		getline(std::cin, Guess);
		Status = BCGame.CheckGuessValidity(Guess);
		switch (Status) 
		{
		case EGuessStatus::WRONG_LENGTH:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n\n";
			break;
		case EGuessStatus::NOT_ISOGRAM:
			std::cout << "Please enter a word without repeating letters.\n\n";
			break;
		case EGuessStatus::NOT_LOWERCASE:
			std::cout << "Please enter all lowercase letters.\n\n";
			break;
		default:
			//assume the guess is valid			
			break;
		}
	} while (Status != EGuessStatus::OK);
	return Guess;
}

bool AskToPlayAgain()
{
	std::cout << "Do you want to play again (y/n)? ";
	FText Response = "";
	getline(std::cin, Response);
	return Response [0] == 'y' || Response[0] == 'Y';
}

void PrintGameSummary()
{
	if (BCGame.IsGameWon()) 
	{
		std::cout << "WELLE DONE - YOU WIN!\n";
	}
	else
	{
		std::cout << "Better Luck next time!\n";
	}
}
